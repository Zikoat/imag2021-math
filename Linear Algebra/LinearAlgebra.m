
%% Forskjellige matrise operasjoner


% definer egen matrise
A = [-7 6 2;-5 4 2;3 -3 -3]

% determinant
det(A)

% inverter matrise
inv(A)

% diagonal matrise
eye(3)

% trekantmatrise
A = triu(
ones(3))

% flip matrise opp-ned
flipud(A)

% flip matrise venstre-høyre
fliplr(A)

% Regn ut A^2633
A^2633

% Regn ut A^-1
A^-1

%% Oppgaver

% Gitt matrisen A
A = [[3,-2];[4,-3]]
% Finn egenverdiene
e = eig(A) 
% merk: dette er ikke en eigenvektor, men en liste av egenverdier

% Finn egenvektorene til denne matrisen
[V,D] = eig(A)
% egenvektor 1
ev1 = V(:,1)/V(1,1)
% egenvektor 2
ev2 = V(:,2)/V(1,2)
% MERK: Vi må nesten alltid skalere egenvektorene slik at de får heltallig
% størrelse. Vi kan gjøre dette ved å dele på ett av elementene. Dette
% krever litt prøving og feiling.

% Gitt matrisen A
A = [-7 6 2;-5 4 2;3 -3 -3]
% gitt egenvektorene v1 og v2 til A
v1 = [0 1 -3]
v1 = [1 1 0]
% finn de 2 tilhørende egenverdiene d1 og d2, og den siste egenvektoren v3
% og egenverdien d3

% Løsning: Vi finner alle egenverdier og egenvektorer
[V, D] = eig(A)
V/V(1,1)
% MERK: D er en diagonal matrise hvor hvert tall på diagonalen tilsvarer
% til en egenvektor. 
% Vi finner egenverdiene igjen i matrisen, og ser hva egenverdien er
% d1 = -1
% d2 = -2
% d3 = -3
% v3 = [1 1 -1]

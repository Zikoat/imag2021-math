%%{
% The powers it should have
n = [-4:4];
% Defines the complex number
z = complex(1,1).^n

plot(z,'x')
grid on
ylim ([-5,5]);
xlim ([-5,5]);
%}
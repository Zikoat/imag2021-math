% Diagonalize
a = sym(input('Matrix: '));
[K, D] = eig(a);
D
K
InvK = inv(K)
% Raised to power
syms n
KDInvKRaisedByPower(n) = K * D^n * InvK
% Custom sequence convergence (to infinity)
% Very brute-force and cannot be used for everything!
% Works best with results that end in 0, 1 or divergent
% Use if no better alternative is present
% This works by approximating the actual value by using large values
% Press CTRL + C to cancel operation
f(n) = sym(input('Enter f(n) sequence: '));
from = input('Start n: ');
to = 10000;
num = sym(0);

for i = from:to
    lastnum = num;
    num = f(i);
    diff = abs(num - lastnum);
    fprintf('%.9f (%d) - Converges towards: %s = %f\n', diff, i, num, num);
    
    if (diff < 0.000000001)
        break;
    end
end
% Calculating BigO of functions
syms x
funcs = sym([
    100*x + 0.01*x^2,
    0.01*x + 100*x^2,
    5+3*x+4*x^2,
    -((x - 1)^3*(12*cos(2) - 8*sin(2)))/6
]); % Not all functions, but can be expanded easily

bigOs = sym([
    1, 
	1/x,
	x,
	x^2,
	x^3,
	x^4,
	x * log(x),
	x * log(x)^2,
	2^x,
	sqrt(x)
]);

for f = 1:length(funcs)
   for bigO = 1:length(bigOs)
       l = funcs(f) / bigOs(bigO);
       if (limit(l, x, inf) < inf)
          fprintf('%s is BigO %s going towards infinity\n', funcs(f), bigOs(bigO));
       end
   end
   fprintf('\n');
   for bigO = 1:length(bigOs)
       l = funcs(f) / bigOs(bigO);
       if (limit(l, x, 0) < inf)
          fprintf('%s is BigO %s going towards 0\n', funcs(f), bigOs(bigO));
       end
   end
   fprintf('---------------\n');
end
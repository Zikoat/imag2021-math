% Multiplicative modular inverse

% Asks the user for input and takes only positive numbers into account
a = input('First number: ');
b = input('Second number: ');

[G, C, ~] = gcd(a,b);
if G==1  % The inverse of a(mod b) exists only if gcd(a,b)=1
    ModMultInv = mod(C,b)
else disp('Modular multiplicative inverse does not exist for these values')
end
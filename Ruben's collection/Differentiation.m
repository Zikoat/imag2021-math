syms x t y
% Differentiation
f(x, t)=t^2*exp(-x)
diff(f) % full diff
diff(f, x) % partial for x
diff(f, y) % partial for y
diff(f, 5) % multiple times diff
diff(f, x, 5) % multiple times partial diff
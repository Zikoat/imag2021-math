% Discrete Dynamical System
% x(k+1) = A*x(k)
% A^k * x0 = c1 * L1^k * v1 + c2 * L2^k * v2 + c3 * L3^k * v3
m = sym(input('Enter eigenvectors + x0 at end (transposed matrix): '));
m = transpose(m)

% Finding c1, c2, c3
% x0 = c1 * v1 + v2 * v2 + c3 * v3
disp('Answers are last column vector:');
m = rref(m)

% ev below are eigenvalues!
% c1(k) = c1 * ev1^k
% c2(k) = c2 * ev2^k
% c3(k) = c3 * ev3^k

% Use limit(f,var,inf) to find limit to infinity
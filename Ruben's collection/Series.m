% Series
syms k n;
f(k) = k^2;
symsum(f, k, 0, 10) % 0 -> 10 for k^2
symsum(f, k, 0, inf) % 0 -> infinity for k^2

g(n) = (n^3-1)/(n^3+1);
symsum(g, n, 1, inf) % goes to infinity

h(n) = 1/(n^2+n^(-2));
symsum(h, n, 1, inf) % converges

i(n) = ((-1)^(n-1)+(-2)^(n-2))/((-3)^(n-3));
symsum(i, n, 1, inf) % 0
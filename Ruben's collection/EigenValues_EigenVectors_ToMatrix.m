%% finn en matrise gitt egenverdier og egenvektorer
egvals = [-2 7];
diagonalEigenvalueMatrix = diag(egvals)

egvec1 = [1; 1];
egvec2 = [2; 3];
D = [egvec1 egvec2] % Her må det være 1 vektor på hver kolonne

A = D * diagonalEigenvalueMatrix * inv(D)
% eig(A) % Sjekk svaret
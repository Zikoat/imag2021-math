% Limits
syms x y
f(x, y) = (x * y) / sqrt(y^2+x^2)
limit(f, 0) % limit based on default variable
limit(f, x, 0) % limit as respects to x
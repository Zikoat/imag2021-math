% Inverse function
syms x
f(x) = x^2;
finverse(f)
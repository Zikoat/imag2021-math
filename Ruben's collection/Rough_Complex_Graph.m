% Rough graph of complex graph
% Supports lower and upper bounds
% to create all sorts of graphs
syms z

%lb(z) = sym(-pi/3); - Task 9
%ub(z) = sym(pi/2); - Task 9
lb(z) = sym(real(z)); % Lower bound - Task 10
ub(z) = sym(inf); % Upper bound - Task 10

%f(z) = angle(z^2); - Task 9
f(z) = imag((-1-1i)*conj(z)); % Task 10

realStart = -2;
realEnd = 2;
imagStart = -2;
imagEnd = 2;
step = 0.1; % Lower step means higher accuracy

figure; 

for r = realStart:step:realEnd
   for im = imagStart:step:imagEnd
      z = r + im * 1i;
      val = f(z);
      
      if val >= lb(z)
          if val <= ub(z)
             plot(z, 'o'); 
             hold on; 
          end
      end
   end
   fprintf('Graph progress: %.0f%%\n', abs((r - realStart) / (realStart - realEnd)) * 100);
end

grid on;
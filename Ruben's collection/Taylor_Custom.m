% Custom taylor (x - x0)
% First n terms + rest + max error calculation

syms x;
f(x) = sym(input('Enter f(x) to taylorize: '));
x0 = input('Enter point x0: ');
n = input('Enter order n: ');
vals = sym('a', [1 n]);
p(x) = sym(0);

for i = 1:n
    g(x) = diff(f, i - 1);
    vals(i) = g(x0);
    p(x) = p(x) + vals(i) * (x - x0)^(i - 1) / factorial(i - 1);
    fprintf('f(%d)(%d) = %s\n', i - 1, x0, vals(i));
end

fprintf('Taylor polynomial p(x): %s\n\n', p(x));

disp('Calculating rest:');
g(x) = diff(f, n);
restval = sym(g(x0));
fprintf('f(%d)(%d) = %s\n', n, x0, restval);
E(x) = restval * (x - x0)^(n) / factorial(n);
fprintf('Rest term E(x): %s\n\n', E(x));

errx = input('Calculate max error for given x value: ');
fprintf('Max error for %s is: %s\n', sym(f(errx)), abs(E(errx)));
% Complex number calculations

% Declare variable
syms z

% Conjugate
conj(z)

% Arg
angle(z)

% Factorization: 
factor(z^2+4*z+5, z, 'FactorMode', 'complex')

% Simplifying
2*i*(i-1)+conj(sqrt(3)+i)^3+(1+i)*conj(1+i)
% or using simplify (expr)

% Solve equation
solve(i+z==1-sqrt(3)*i*z, z)
solve(z*(z-2*i)==3, z)
solve(z^6==-64, z)

% Create graphs
n=[-4;-3;-2;-1;0;1;2;3;4]
(1+i).^n
plot(ans)


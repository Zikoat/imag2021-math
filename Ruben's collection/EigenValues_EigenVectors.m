% Eigenvalues and eigenvectors
a = sym(input('Matrix: '));
[ev1, ev2] = eig(a);

disp('Eigenvalues: ');
disp(ev2);

disp('Eigenvectors: ');
disp(transpose(ev1));
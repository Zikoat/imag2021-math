% Author: Anders Oliversen
% Oppgave 2
clc, clear

n = 0;
for a = 5:5000
    if (gcd(1001,a) == 1)
        n = n +1;
    end  
end
disp(n)

%%
% Oppgave 3b

clc, clear

for i = 1:103
    D = mod(23*i,103);
    if (D == 30)
        disp(i)
    end  
end

%%
% Oppgave 4d
clc, clear

for i = 1:6377
    D = mod(1000*i,6377);
    if (D == 1)
        disp(i)
    end  
end


%%
% Oppgave 8b)
clc, clear

for d = 0:40
    phi = mod(d*7,40);
    if (phi == 1)
        disp(d)
    end  
end

%%
% Oppgave 8d)
clc, clear

for n = 0:55
    c = mod(n^7,55);
    if (c == 39) % M� sjekke for hvert tall i budskapet: 39,5,17,5,9,1
        disp(n)
    end  
end

% Gir 29 15 18 15 14 1 ... = "Corona" n�r engelsk alfabet brukes.

# Imag2021 Matlab

## Matlab
_kommer_

[Matlab Cheatsheet](https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/47533/versions/1/screenshot.jpg) 

## Markdown
Bruk LaTeX ved å skrive $`x`$ som ``$`x`$``, eller bruk multiline math blocks
```math
f(x)=x^2\\
f:R\to R
```

## Hjelpemidler

### Formelark
* [Formelsamling Matematiske metoder 2 2020](Formelsamling Matematiske metoder 2 2020.pdf)
* [PowerPoint fra forelesning](https://studntnu-my.sharepoint.com/personal/kjellemo_ntnu_no/_layouts/15/Doc.aspx?sourcedoc={0bd291fb-f85f-4f94-b083-7ed8e915f1e8}&action=view&wd=target%28forelesninger.one%7C064e3878-7b78-4e06-8d24-59356051873c%2FFolger_rekker_2%7C49c90872-4f44-ca42-a969-d14e3454eb5f%2F%29)

### Generelle verktøy
* [Online Matlab](https://matlab.mathworks.com/) (logg inn/sign up med ntnu bruker)
* Online [Geogebra 3D](https://www.geogebra.org/3d?lang=en) og [Geogebra 2D](https://www.geogebra.org/graphing?lang=en)
* [Online Python compiler](https://repl.it/languages/python3)
* [Online Jupyter notebook](https://mybinder.org/v2/gh/ipython/ipython-in-depth/master?filepath=binder/Index.ipynb)
* [Wolfram notebooks](https://www.wolframcloud.com/) (Opprett gratis bruker)
* [approach0](https://approach0.xyz/) lar deg finne relevante oppgaver(ofte med løsning) fra latex eller keyword ([eksempel med induksjon](https://approach0.xyz/search/?q=%24P(n)%3A%201%5E%7B3%7D%2B2%5E%7B3%7D%2B3%5E%7B3%7D%2B%5Cldots%2Bn%5E%7B3%7D%3D%5Cfrac%7Bn%5E%7B2%7D(n%2B1)%5E%7B2%7D%7D%7B4%7D%24&p=1))
* [mathpix](https://mathpix.com/) lar deg enkelt få ut latex fra bilde av en likning, benyttes sammen med approach0.

### Complex numbers
* [Dave's Short intro on Complex numbers](https://www2.clarku.edu/faculty/djoyce/complex/mult.html)

### linear algebra
* [Matrix calculator](https://matrixcalc.org/no/)
* [Transformasjoner visualisering](https://web.ma.utexas.edu/users/ysulyma/matrix/) hvis du trenger å rotere/speile/scale transformasjoner visuellt

### Funksjoner med flere variable
* [3D plotter](https://www.monroecc.edu/faculty/paulseeburger/calcnsf/CalcPlot3D/)
* [Limits cheat sheet](https://www.studocu.com/en-us/document/university-of-california-santa-cruz/calc/coursework/calculus-cheat-sheet-limits-reduced/3642726/view)
* Finn en likning for tangentplanet til flaten z=f(x,y) i et punkt: [Wolfram Alpha widget](https://www.wolframalpha.com/widgets/view.jsp?id=570e146d063102ee8809bd7ca452c246)

### Rekker
* [Rekke kalkulator](https://www.symbolab.com/solver/series-calculator)

### Logikk og Påstander
* [Truth Table Generator ](https://web.stanford.edu/class/cs103/tools/truth-table-tool/) lar deg lage sannhetstabell raskt og enkelt. Funker bra med "Konstruer logiske utsagn"
* [Konstruer logiske utsagn](https://www.erpelstolz.at/gateway/formular-uk-zentral.html)
* [Logikk, rask gjennomgang](http://kristofferdahl.no/public/Logikk.html#utsagn_)

### Mengder
* Lag venndiagram av Set på [WolframAlpha](https://www.wolframalpha.com/input/?i=A+intersect+%28A+union+B%27%29) og [flere eksempler](https://www.wolframalpha.com/examples/mathematics/logic-and-set-theory/)

### Talllære
* [Eulers totient funksjon](http://www.javascripter.net/math/calculators/eulertotientfunction.htm)
* [Divisorer til et tall](https://en.wikipedia.org/wiki/Table_of_divisors)
* [Primfaktorisering](https://www.calculatorsoup.com/calculators/math/prime-factors.php)

### Grafteori
* [Create graphs](https://graphonline.ru/en/)

### RSA
* [About rsa](https://www.di-mgt.com.au/rsa_alg.html)
* [RSA encryption generator](http://encryption-calc.herokuapp.com/)
### Kombinatorikk
* [Permutation calculator](https://www.mathsisfun.com/combinatorics/combinations-permutations-calculator.html)
